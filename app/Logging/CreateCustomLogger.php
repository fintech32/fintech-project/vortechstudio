<?php

namespace App\Logging;

use Monolog\Logger;

class CreateCustomLogger
{

    /**
     * Create a custom Monolog instance.
     *
     * @param  array $config
     *
     * @return \Monolog\Logger
     */
    public function __invoke(array $config): Logger
    {
        return new Logger(
            config("app.name"),
            [
                new MattermostHandler(
                    $config['url'],
                    $config['level']
                ),
            ]
        );
    }

}
