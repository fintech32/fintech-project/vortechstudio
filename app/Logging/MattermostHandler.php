<?php

namespace App\Logging;

use Monolog\LogRecord;
use GuzzleHttp\Client;
use Monolog\Logger;
use Log;

class MattermostHandler extends \Monolog\Handler\AbstractProcessingHandler
{
    private $webHookUrl;
    private $client;
    public function __construct($webHookUrl, $level = Logger::DEBUG, $bubble = true, $client = null)
    {
        parent::__construct($level, $bubble);

        $this->webHookUrl = $webHookUrl;
        $this->client     = ($client) ?: new Client();
    }

    /**
     * @inheritDoc
     */
    protected function write(LogRecord $record): void
    {
        // TODO: Implement write() method.
        $this->client->request('POST', $this->webHookUrl, [
            'form_params' => [
                'payload' => json_encode(
                    [
                        'username' => config('mattermost.username'),
                        'icon_url' => config('mattermost.icon'),
                        'text'     => $record['message'],
                    ]
                ),
            ],
        ]);
    }
}
