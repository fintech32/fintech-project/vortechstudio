<?php
return [
    "username" => env("MATTERMOST_USERNAME"),
    "icon" => env("MATTERMOST_ICON"),
    "log_url" => env("MATTERMOST_LOG_URL"),
    "log_helpdesk" => env("MATTERMOST_HELPDESK_URL")
];
